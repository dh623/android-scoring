#!/bin/bash
set -e;

sudo docker build -t android-scoring-extractor:latest -f docker/android-scoring-extractor/Dockerfile .;
sudo docker build -t android-scoring-dekra:latest -f docker/android-scoring-dekra/Dockerfile .;
echo "[+] Built all the dockers"
