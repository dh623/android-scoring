#!/bin/bash
set -e;

# Download a Pixel 8 and a Pixel 6a image
mkdir -p data/images/;
wget -P data/images/ https://dl.google.com/dl/android/aosp/husky-ud1a.230803.022.a3-factory-a95417f6.zip;
wget -P data/images/ https://dl.google.com/dl/android/aosp/bluejay-tq3a.230805.001-factory-49a03cb1.zip;
echo "[+] Downloaded the Google images"
