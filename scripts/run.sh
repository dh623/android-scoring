#!/bin/bash
set -e;

IMAGES=(
    "husky-ud1a.230803.022.a3-factory-a95417f6"
    "bluejay-tq3a.230805.001-factory-49a03cb1"
);

# Build all the dockers
./scripts/docker_build_all.sh;

# Ensure analysis folders exist and are clean
mkdir -p data/intermediate/_tmp;
rm -rf data/intermediate/*;
mkdir -p data/intermediate/extracted;
echo "[+] Cleaned the intermediate folders"

# Prepare
for IMAGE in "${IMAGES[@]}"; do
  # Extract boot images
  sudo docker run -v "$(pwd)/data/images/:/images" -it android-scoring-extractor:latest "/images/$IMAGE.zip" google;
  mv "data/images/$IMAGE.zip.extracted" "data/intermediate/extracted/$IMAGE";
  echo "[+] Extracted the boot images for $IMAGE";

  # Collect the APK files
  mkdir -p "data/intermediate/apks/$IMAGE/";
  find "data/intermediate/extracted/$IMAGE" -name "*.apk" -exec cp -v {} "data/intermediate/apks/$IMAGE/" \;
  echo "[+] Collected the APK files for $IMAGE:";

  # Move APK files into individual folders
  pushd "data/intermediate/apks/$IMAGE/";
  ls -la;
  for apk in *.apk; do
      echo " - ${apk%.*}"
      mkdir -p "${apk%.*}";
      mv "$apk" "${apk%.*}/";
  done;
  popd;
done;

# Make docker a bit less flaky on Ubuntu 23.10
sudo service docker restart

# Setup MYSQL for the Dekra script
sudo docker container rm -f db || true;
sudo docker network rm -f dekra || true;
echo "[+] Removed old containers and networks"

sudo docker network create dekra;
sudo docker run -it -e MYSQL_RANDOM_ROOT_PASSWORD=1 -e MYSQL_USER=masa_script -e MYSQL_PASSWORD=MASA123 -e MYSQL_DATABASE=automated_MASA --name db --network dekra -d mysql:8
echo "[+] Created the network and started the MySQL database"

## Setup the Derka database (initial wait required for service discovery to wake up)
sudo docker run --rm --network dekra --name dekra-setup -it android-scoring-dekra:latest python3 -c "import time; time.sleep(15); from db.database_utils import first_execution; first_execution()"
echo "[+] Created the Dekra database"

# Run the Dekra script
for IMAGE in "${IMAGES[@]}"; do
  sudo docker run -v "$(pwd)/data/intermediate/apks/$IMAGE:/usr/src/app/apks" --rm --network dekra --name dekra-setup -it android-scoring-dekra:latest /bin/bash automate_apps_updated 1
  echo "[+] Ran the Dekra script"
done;
