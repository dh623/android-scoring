#!/bin/bash
set -e;

git submodule init;
git submodule update;
echo "[+] Initialized submodules"

pushd submodules/DekraScripts;
git reset --hard main;
git apply ../DekraScripts.patch;
popd;
echo "[+] Patched the DekraScripts submodule"
