# Data folder

This folder stores all data used during analysis.

In particular, it contains the following subfolders:

- `images` contains the downloaded images. It is typically not committed.
- `intermediates` contains the intermediate data files. It is typically not committed.
- `analysis` contains the analysis and results of the main run.
