# Android scoring framework

This repository contains a prototype implementation that combines the firmware image extraction tool with the Dekra scripts.
It's mostly running on duct-tape and Docker containers that are orchestrated by bash scripts.
The goal is that full runs can be executed with few commands on remote servers.

## Setup (1h)

For the first installation clone the repository, initialize the submodules (one will be patched with our changes), and download the default firmware images (Google Pixel).

```bash
git clone git@github.com:lambdapioneer/android-scoring.git
cd android-scoring
./scripts/first_time_submodule_setup.sh
./scripts/download_sample_google_images.sh
```

## Running (many hours)

The main script is `./scripts/run.sh`.
Internally it will build the Docker containers (if not already done), and then for each image first extract all APKs and then analyze them individually.

```bash
./scripts/run.sh
```

## Other

#### Design decisions

tbd

#### TODOs

tbd

#### Helpful commands

Delete all existing Docker things (images, containers, volumes, networks):

```bash
docker system prune -a
```